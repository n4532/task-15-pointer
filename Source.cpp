#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include "string.h"

using namespace std;

void replaceLine(char*, char*, char*);
void getReplaceTests(char*);
void getUserMode(char*);
void getReplacedLine(char*, char*, char*);

int main()
{
	const int size = 256;
	char source[size] = "Lorem lorem   Ipsum, is simply  79 dummy text of the"
		" printing - and typesetting : % simply. Lorem Ipsum has been"
		" the industry's , simply dummy text ever since the 1500s, simply";

	while (true) {
		int n;
		cout << "Press 1 to enable test mode" << endl;
		cout << "Press 2 to replace your words" << endl;
		cout << "Press 3 to exit" << endl;
		cin >> n;
		system("cls");

		switch (n)
		{
		case 1:
			getReplaceTests(source);
			return 0;
			system("pause");
			break;
		case 2:
			getUserMode(source);
			return 0;
			break;
		case 3:
			return 0;
		default:
			break;
		}
	}

	return 0;
}

void replaceLine(char* source, char* replacementLine, char* newLine)
{
	int replacementLength = strlen(replacementLine),
		newLength = strlen(newLine);
	char* pword = strstr(source, replacementLine);
	char* p = pword + replacementLength;
	strcpy(newLine + newLength, p);
	strcpy(pword, newLine);

}

void getReplaceTests(char* source)
{
	const int size = 86;
	int count = 0;

	ifstream replacedLines, newLines;
	replacedLines.open("replacedLines.txt");
	newLines.open("newLines.txt");

	if (!replacedLines.is_open() || !newLines.is_open()) {
		cout << "Cannot open this file";
		system("pause");
	}

	while (!newLines.eof() && !replacedLines.eof()) {
		char* replacedLine = new char[size];
		char* newLine = new char[size];
		replacedLines.getline(replacedLine, size);
		newLines.getline(newLine, size);

		getReplacedLine(source, replacedLine, newLine);
	}

	replacedLines.close();
	newLines.close();

}

void getUserMode(char* source)
{
	int size = 86;
	char* replacedLine = new char[size];
	char* newLine = new char[size];


	while (true) {
		cout << "Enter the old line: " << endl;
		cin.getline(replacedLine, size);
		system("cls");

		if (replacedLine[0] != '\0') {
			break;
		}
	}

	while (true) {
		cout << "Enter the new line: " << endl;
		cin.getline(newLine, size);
		system("cls");

		if (replacedLine[0] != '\0') {
			break;
		}
	}

	getReplacedLine(source, replacedLine, newLine);

}

void getReplacedLine(char* source, char* replacedLine, char* newLine)
{
	cout << "This is an original line: " << endl << source << endl << endl;
	cout << "This is a new line:";

	replaceLine(source, replacedLine, newLine);
	cout << endl << source << endl << endl;
}
